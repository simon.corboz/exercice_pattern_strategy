import java.util.Arrays;

public class Main {
    public static void main(String[] args){
        int[] arr = new int[10000];
        for (int i = 0; i < arr.length; i++) {
            arr[i] = (int)(Math.random() *10000);
        }

        int[] deepArrCopyForSelectionSort = arr.clone();
        int[] deepArrCopyForInsertionSort = arr.clone();
        int[] deepArrCopyStandardLibrarySort = arr.clone();

        //La première tache consiste à créer une interface de stratégie
        //la signature de la méthode de tri doit être la suivante: void(int[])

        //la deuxième tache consiste à créer les 3 classes de stratégies

        //la troisième tache consiste à éditer la classe Context

        //la dernière tâche consiste à éditer le main pour tester les algorithmes de tri
        assert(Arrays.equals(deepArrCopyForSelectionSort,deepArrCopyForInsertionSort));
        assert(Arrays.equals(deepArrCopyForInsertionSort,deepArrCopyStandardLibrarySort));

    }
}
