import java.util.concurrent.TimeUnit;

public class Context {
    private int[] array;

    public int[] getArray() {
        return array;
    }

    public void setArray(int[] array) {
        this.array = array;
    }
    /**
     *
     * Mettre les attributs et les getter setter ici en sachant que:
     * -Un contexte a une stratégie (composition)
     * -on peut modifier la stratégie du contexte a tout moment
     * -on aimerait connaitre la stratégie du contexte a tout moment
     */
    public Context(int[] array, /*????*/){
        this.array = array;
        //this.? = ?
    }
    public void sort(){
        long start = System.nanoTime();
        //que mettre ici?
        long end = System.nanoTime();
        long timeInMillis = TimeUnit.MILLISECONDS.convert(end - start, TimeUnit.NANOSECONDS);
        System.out.println("Time spend in ms: " + timeInMillis);
    }
}
